<?php
// Array Number
// $data=array('anggur','mangga','melon');
// print_r($data);

// // Array assosiative
// $data=array(
// 'buah1'=>'anggur',
// 'buah2'=>'mangga',
// 'buah3'=>'melon'
// );
// print_r($data);

// // Array Multi Dimensional
$data=array(
    array('anggur','mangga','melon'),
    array('jeruk','apel','jambu'),
    array('nanas','pisang','pir')
);
print_r($data);

// Multi Dimensional Array Assosiatif
// $data=array(
//     array(
//         'buah'=>'mangga',
//         'pohon'=>'tua',
//         'jenis'=>array(
//             'batang'=>'keras',
//             'berdaun'=>'lembek',
//             'berkulit'=>array(
//                         'tajam',
//                         'tumpul',
//                         'lancip')

//             )
//             ),
//     array(
//         'buah'=>'mangga',
//         'pohon'=>'tua',
//         'jenis'=>array(
//             'batang'=>'keras',
//             'berdaun'=>'lembek',
//             'berkulit'=>array(
//                 'tajam',
//                 'tumpul',
//                 'lancip')
//         )
//     )
//             );
//     print_r($data);
?>